//
//  dictionaryData.h
//  tataPendu
//
//  Created by Tatadmound on 28/02/2021.
//
typedef struct DictionaryData DictionaryData;
struct DictionaryData{
    //'b' for basic dictionary mod
    char dictionaryMod;
    //number of words presents in the dictionary (of the active mod)
    int wordsInDictionary;
    //3 Status
    //0 -> dictionary file founded
    //in case of dictionary file not founded
        //1 -> create a default new one : newBasicDictionary()
        //2 -> the user will provide a personal dictionary file : init()
    char basicDictionaryStatus;
    FILE* dictionary;
};

#ifndef dictionaryData_h
#define dictionaryData_h


#endif /* dictionaryData_h */
