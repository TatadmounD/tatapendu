//
//  gameData.h
//  tataPendu
//
//  Created by Tatadmound on 20/02/2021.
//
typedef struct GameData GameData;
struct GameData{
    //number of healtPoint in the start of the game
    //as float to manage the pending animation in promptTry()
    int healthGauge;
    int healthPoints;
    // char array, lenght of healthPoints made to show the wrong answers history
    char* guessHistory;
    //3 status :
    //'r' -> retry : tryAgain()
    //'w' -> won & 'o' -> lost : manageEOTry()
    char gameStatus;
    //set in findWordInBasicDictionary()
    int wordLength;
    //user's attempt
    char guess;
    //binary
    //1->good answer //2->bad answer
    //in correct()
    int guessStatus;
    //prompt the word as "******"
    //every good answer will replace star(s)
    //in correct()
    char *secretWord;
    //the word to find
    //set in findWordInBasicDictionary()
    char *secretPrompt;
};
#ifndef gameData_h
#define gameData_h


#endif /* gameData_h */
