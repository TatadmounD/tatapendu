//
//  end.c
//  tataPendu
//
//  Created by Tatadmound on 20/02/2021.
//
#include "../../Headers/gameMaster.h"
#include "../../Headers/utilities.h"
#include <ctype.h>

void endGame(GameData *gameDataPointer, DictionaryData *dictionaryDataPointer){
    if (gameDataPointer->gameStatus=='w') {
#ifdef WINDOWS
        printf("\nGagne ! Le mot secret etait bien : ");
#endif
#ifdef UNIX
        printf("\nGagné ! Le mot secret était bien : ");
#endif
        promptSecret(gameDataPointer);
    }else{
        printf("\n     |--|     \n     O  |     \n    /|\\ |     \n     -  |     \n    / \\ |     \n        |     \n     ------   \n");
#ifdef WINDOWS
            printf("\nPerdu ! Il ne vous reste plus aucun coup a jouer ..");
#endif
#ifdef UNIX
        printf("\nPerdu ! Il ne vous reste plus aucun coup à jouer ..");
#endif
    }
    printf("\nRejouer?");
    char binaryInput=askBinaryInput();
    if (binaryInput=='O' || binaryInput=='0') {
        printf("Lancement d'une nouvelle partie\n");
        gameMaster();
    }else{
        printf("Aurevoir.\n");
        free(gameDataPointer->secretWord);
        free(gameDataPointer->secretPrompt);
        free(gameDataPointer->guessHistory);
        free(gameDataPointer);
        free(dictionaryDataPointer);
        exit(0);
    }
}
