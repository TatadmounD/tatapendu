//
//  init.c
//  tataPendu
//
//  Created by Tatadmound on 20/02/2021.
//

#include <stdlib.h>
#include <string.h>
#include "../../Headers/gameMaster.h"
#include "../../Headers/utilities.h"

char init(GameData *gameDataPointer, DictionaryData *dictionaryDataPointer){
    if (gameDataPointer!=NULL) {
        
        //Outputs
        printf("Bienvenue dans le Pendu !\n");
        if (dictionaryDataPointer->basicDictionaryStatus=='1' || dictionaryDataPointer->basicDictionaryStatus=='2') {
            confirmBasicDictionaryStatusToUser(gameDataPointer, dictionaryDataPointer);
        }
        
        //init dictionaryData struct
        dictionaryDataPointer->dictionaryMod='b';
        dictionaryDataPointer->dictionary=NULL;
        initDictionary(gameDataPointer, dictionaryDataPointer);
        randomWord(dictionaryDataPointer, gameDataPointer);
        
        //init gameData struct
        float healthPointsFromTxt=10;
        gameDataPointer->healthGauge = healthPointsFromTxt;
        gameDataPointer->healthPoints = healthPointsFromTxt;
        gameDataPointer->guessHistory = malloc(sizeof(char)*gameDataPointer->healthPoints);
        for(int healthPointsIndex=0; healthPointsIndex<gameDataPointer->healthPoints; healthPointsIndex++){
            *(gameDataPointer->guessHistory+healthPointsIndex)=0;
        }
        gameDataPointer->gameStatus = 'c';
        gameDataPointer->guess = '0';
        gameDataPointer->guessStatus = 0;
        
        return 0;
    }else{
        printf((DEV_MOD)? "erreur malloc! Arrêt du programme imminent\n" : "une erreur est survenue\ncode: ???\n" );
        free(gameDataPointer);
        exit(0);
    }
}

void confirmBasicDictionaryStatusToUser(GameData* gameDataPointer, DictionaryData* dictionaryDataPointer){
    FILE* dictionary = fopen("dictionnaire_b.txt", "r");
    if (dictionary!=NULL) {
#ifdef WINDOWS
        printf((dictionaryDataPointer->basicDictionaryStatus=='1')? "Le dictionnaire par defaut a bien ete cree.\n" : "Votre dictionnaire perso a bien ete pris en compte.\n" );
#endif
#ifdef UNIX
        printf((dictionaryDataPointer->basicDictionaryStatus=='1')? "Le dictionnaire par défaut a bien été créé.\n" : "Votre dictionnaire perso a bien été pris en compte.\n" );
#endif
    }
    fclose(dictionary);
}

char initDictionary(GameData *gameDataPointer, DictionaryData *dictionaryDataPointer){
    //dictionary mod redirection
    switch (dictionaryDataPointer->dictionaryMod) {
        case 'b':
            dictionaryDataPointer->dictionary=fopen("dictionnaire_b.txt", "r");
            initBasicDictionary(gameDataPointer, dictionaryDataPointer);
            break;
            
        default:
            break;
    }
    return 0;
}

char initBasicDictionary(GameData *gameDataPointer, DictionaryData *dictionaryDataPointer){
    //check if the file does exist
    if (dictionaryDataPointer->dictionary != NULL) {
        dictionaryDataPointer->basicDictionaryStatus = '0';
        fclose(dictionaryDataPointer->dictionary);
        //delete unwanted words and recount them
        cleanBasicDictionary(dictionaryDataPointer->dictionary, gameDataPointer, dictionaryDataPointer);
        updateWordsCountInBasicDictionary(dictionaryDataPointer->dictionary, dictionaryDataPointer);
    }else{
#ifdef WINDOWS
        printf("Dictionnaire de mots introuvable. Vous pouvez le remplacer par votre propre dictionnaire en le placant dans le dossier du jeu (penser a bien le nommer 'dictionnaire_b.txt'). Sinon vous pouvez jouer avec le dictionnaire par defaut.\n1: Continuer avec le dictionnaire par defaut\n2: J'ai bien copie mon dictionnaire la ou il faut :D\n");
#endif
#ifdef UNIX
        printf("Dictionnaire de mots introuvable. Vous pouvez le remplacer par votre propre dictionnaire en le plaçant dans le dossier du jeu (penser à bien le nommer 'dictionnaire_b.txt'). Sinon vous pouvez jouer avec le dictionnaire par défaut.\n1: Continuer avec le dictionnaire par défaut\n2: J'ai bien copié mon dictionnaire là où il faut :D\n");
#endif
        //set user's answer and relaunch init() to check again if the file does exist
        char *userInput=malloc(sizeof(char));
        *userInput=askFigureInput('1', '2');
        dictionaryDataPointer->basicDictionaryStatus=*userInput;
        free(userInput);
        if (dictionaryDataPointer->basicDictionaryStatus=='1') { //remplace userInput
            newDefaultBasicDictionary();
        }
        init(gameDataPointer, dictionaryDataPointer);
    }
    return 0;
}

char cleanBasicDictionary(FILE* basicDictionary, GameData* gameDataPointer, DictionaryData* dictionaryDataPointer){
    //rename our basicDictionary
    rename("dictionnaire_b.txt", "dictionnaire_f.txt");
    //create our future clean basicDictionary
    fclose(dictionaryDataPointer->dictionary);
    dictionaryDataPointer->dictionary=fopen("dictionnaire_b.txt", "w+");
    basicDictionary = fopen("dictionnaire_f.txt", "r+");
    rewind(basicDictionary);
    
    char *variables;
    variables=malloc(sizeof(char)*5);
    do {
        *variables=fgetc(basicDictionary);
        //printf("%c", *variables);
        if (*variables!=EOF) {
            *(variables+3)+=1;
        }
        if (*variables==' ') {
            *(variables+1)=1;
        }else if(*(variables+1)>0){
            *(variables+2)=1;
        }
        if (*variables=='\n' || *variables==EOF) {
            //word is too long
            if (*(variables+3)>25) {
                if (DEV_MOD) {
                    printf("ligne n°%d: Le mot est trop long\n", *(variables+4));
                }
            }
            //the word is too short
            else if (((*(variables+3))-(*(variables+1)))<4 && (*(variables+4))>1) {
                if (DEV_MOD) {
                    printf("ligne n°%d: Le mot est trop court\n", *(variables+4));
                }
            }
            //there is a space between letters
            else if (*(variables+2)) {
                if (DEV_MOD) {
                    printf("ligne n°%d:  Il y a un espace entre deux lettres\n", *(variables+4));
                }
            }
            //so this line is a good one
            else{
                if (DEV_MOD) {
                    printf("ligne n°%d: pas de problème\n", *(variables+4));
                }
                fseek(basicDictionary, -(*(variables+3)), SEEK_CUR);
                do {
                    //copy this line
                    *variables=fgetc(basicDictionary);
                    //dont print the eof char
                    if (*variables!=EOF) {
                        if (DEV_MOD) {
                            printf("%c", *variables);
                        }
                        fputc(*variables, dictionaryDataPointer->dictionary);
                        fflush(stdout);
                    }
                    *(variables+3)=0;
                } while (*variables!='\n' && *variables!=EOF);
            }
            *(variables+4)+=1;
            *(variables+3)=*(variables+2)=*(variables+1)=0;
        }
    } while (*variables!=EOF);
    free(variables);
    fclose(basicDictionary);
    
    //apply writing & re-open in r+ mode
    fflush(stdout);
    fclose(dictionaryDataPointer->dictionary);
    dictionaryDataPointer->dictionary=fopen("dictionnaire_b.txt", "r+");
    remove("dictionnaire_f.txt");
    return 0;
}

char updateWordsCountInBasicDictionary(FILE* dictionary, DictionaryData *dictionaryDataPointer){
    //get the numbers of words presents and add some to have at least 20 words in it
    //the first line isn't a word
    dictionaryDataPointer->wordsInDictionary=(countLinesInFile(dictionaryDataPointer->dictionary))-1;
    if (dictionaryDataPointer->wordsInDictionary<20) {
        addWordsToBasicDictionary(dictionaryDataPointer->dictionary,dictionaryDataPointer->wordsInDictionary, dictionaryDataPointer);
        dictionaryDataPointer->wordsInDictionary=20;
    }
    long *startPosition=malloc(sizeof(long));
    *startPosition=getLinePosition(dictionaryDataPointer->dictionary, 2);

    //update the first line
    fclose(dictionaryDataPointer->dictionary);
    rename("dictionnaire_b.txt", "dictionnaire_f.txt");
    FILE* dictionaryToUpdate=NULL;
    dictionaryDataPointer->dictionary=fopen("dictionnaire_b.txt", "w+");
    dictionaryToUpdate=fopen("dictionnaire_f.txt", "r");
    
    fprintf(dictionaryDataPointer->dictionary, "%d\n", dictionaryDataPointer->wordsInDictionary);
    fflush(stdout);
    copyTheRestOfTheFile(dictionaryToUpdate, dictionaryDataPointer->dictionary, *startPosition);
    free(startPosition);
    fflush(stdout);
    fclose(dictionaryDataPointer->dictionary);
    remove("dictionnaire_f.txt");
    
    fflush(stdout);
    fclose(dictionaryToUpdate);
    remove("dictionnaire_f.txt");
    dictionaryDataPointer->dictionary=fopen("dictionnaire_b.txt", "r+");
    return 0;
}

void newDefaultBasicDictionary(){
    char words[]="380\nBISET\nDOIGT\nJURON\nKOALA\nNABOT\nQUART\nSTYLO\nBIVOUAC\nCHARIOT\nJACOBIN\nMILORDS\nNUNUCHE\nUNICITE\nZIGOMAR\nFINAUDERIE\nGOUJATERIE\nKICHENOTTE\nNYCTHEMERE\nPURGATOIRE\nSPHERICITE\nTABULATION\nVADROUILLE\nYPONOMEUTE\nDEAMBULATION\nFRISTOUILLER\nGYROSTATIQUE\nLOCALISATION\nMADEMOISELLE\nOSSIFICATION\nROUFLAQUETTE\nSUPERSONIQUE\nCONFIDENTIALITE\nEQUIMOLECULAIRE\nFALLACIEUSEMENT\nINDEBOULONNABLE\nKALEIDOSCOPIQUE\nRABOUGRISSEMENT\nVIDEOPROJECTEUR";
    //char words[]="28\nKO A\nJURON\nNA\nQUART\nBISET"; //debug
    FILE* dictionary = NULL;
    dictionary = fopen("dictionnaire_b.txt", "w+");
    if (dictionary!=NULL) {
        fputs(words, dictionary);
        fflush(stdout);
        fclose(dictionary);
    }
}

char addWordsToBasicDictionary(FILE* dictionary, int wordsCount, DictionaryData* dictionaryDataPointer){
    rewind(dictionaryDataPointer->dictionary);
    //*variables [wordsAdded, wordsToAddIndex, numberOfWordsToAdd]
    //*variables=>wordsAdded/*(variables+1)=>wordsToAddIndex/*(variables+2)=>numberOfWordsToAdd
    char *variables=malloc(sizeof(char)*2);
    *variables=0;
    *(variables+1)=20-wordsCount;
    int *wordsToAddIndex=malloc(sizeof(int));
    *wordsToAddIndex=0;
    char wordsToAdd[]="\nPUANT\nFERTILE\nABRACADABRAS\nBILATERAUX\nAUDIOCONFERENCE\nYACHT\nLEOPARD\nLUMINIFERE\nJUXTAPOSABLE\nBACTERIOLOGIQUE\nCUBIS\nKREMLIN\nROYALEMENT\nOSSIFICATION\nQUOTIDIENNEMENT\nXENON\nIVRESSE\nSPHERICITE\nNOTIFICATION\nKALEIDOSCOPIQUE\n\n";
    
    fseek(dictionary, 0, SEEK_END);
    do {
        fputc(wordsToAdd[*wordsToAddIndex], dictionaryDataPointer->dictionary);
        if (wordsToAdd[*wordsToAddIndex]=='\n') {
            *variables+=1;
        }
        *wordsToAddIndex+=1;
    } while (*variables<=*(variables+1));
#ifdef WINDOWS
    printf("\n%d mots ont ete ajoutes a votre dictionnaire.\n", *(variables+1));
#endif
#ifdef UNIX
    printf("\n%d mots ont été ajoutés à votre dictionnaire.\n", *(variables+1));
#endif
    fclose(dictionaryDataPointer->dictionary);
    dictionaryDataPointer->dictionary=fopen("dictionnaire_b.txt", "r+");
    free(variables);
    free(wordsToAddIndex);
    return 0;
}

int randomWord(DictionaryData *dictionaryDataPointer, GameData *gameDataPointer){
    switch (dictionaryDataPointer->dictionaryMod) {
        case 'b':
            findWordInBasicDictionary(randomWordInBasicDictionary(dictionaryDataPointer), gameDataPointer, dictionaryDataPointer);
            break;
            
        default:
            break;
    }
    return 0;
}

int randomWordInBasicDictionary(DictionaryData *dictionaryDataPointer){
    return rand() % (dictionaryDataPointer->wordsInDictionary-1);
}

char findWordInBasicDictionary(int wordId, GameData *gameDataPointer, DictionaryData* dictionaryDataPointer){
    FILE* dictionary=NULL;
    dictionary=fopen("dictionnaire_b.txt", "r");
    //variables [currentChar,wordLength,wordIndex]
    //*variables=>currentChar/*(variables+1)=>wordLength/*(variables+2)=>wordIndex
    char *variables=malloc(sizeof(char)*3);
    *variables=0;
    *(variables+1)=-1;
    *(variables+2)=0;

    *(variables+1)=(getLineLength(dictionaryDataPointer->dictionary, wordId+2))-1;
    
    gameDataPointer->wordLength = *(variables+1);
    gameDataPointer->secretPrompt = malloc(sizeof(char)*gameDataPointer->wordLength); //to start like as "******"
    gameDataPointer->secretWord = malloc(sizeof(char)*gameDataPointer->wordLength); //our word to find
    
    //set the position in the file
    fseek(dictionaryDataPointer->dictionary, getLinePosition(dictionaryDataPointer->dictionary, wordId+2), SEEK_SET);
    
    
    for (*(variables+2)=0; *(variables+2)<*(variables+1); *(variables+2)+=1) {
        *variables=getc(dictionaryDataPointer->dictionary);
        *(gameDataPointer->secretWord+*(variables+2))=*variables;
        *(gameDataPointer->secretPrompt+*(variables+2))='*';
    }
    free(variables);
    return 0;
}
