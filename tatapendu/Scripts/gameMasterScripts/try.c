//
//  try.c
//  tataPendu
//
//  Created by Tatadmound on 20/02/2021.
//
#include "../../Headers/gameMaster.h"
#include "../../Headers/utilities.h"
#include <ctype.h>

char tryAgain(GameData *gameDataPointer){
    gameDataPointer->guessStatus=0; //by default 0, correct() turn it to 1 if any match
    //output
    promptTry(gameDataPointer);
    //return input's char
    gameDataPointer->guess = getUpperChar();
    //work on (*gameDataPointer).guessStatus & (*gameDataPointer).secretPrompt
    correct(gameDataPointer);
    //if that's a bad answer
    if (!gameDataPointer->guessStatus) {
        //add the guess to the bad answers history
        *(gameDataPointer->guessHistory+(10-gameDataPointer->healthPoints))=gameDataPointer->guess;
        gameDataPointer->healthPoints-=1;
    }
    //work on (*gameDataPointer).gameStatus
    if (!manageEOTry(gameDataPointer)) {
        return 0;
    }
    return 0;
}
char promptTry(GameData *gameDataPointer){
    //Hanging animation
    int healthRatioModulo = (gameDataPointer->healthPoints)%(gameDataPointer->healthGauge);
    if (healthRatioModulo==0) {
        printf("\n              \n              \n              \n              \n              \n              \n              \n");
    }else if (healthRatioModulo<=1) {
        printf("\n     |--|     \n     O  |     \n    /|\\ |     \n     -  |     \n    /   |     \n        |     \n     ------   \n");
    }else if (healthRatioModulo<=2) {
        printf("\n     |--|     \n     O  |     \n    /|\\ |     \n     -  |     \n        |     \n        |     \n     ------   \n");
    }else if(healthRatioModulo<=3){
        printf("\n     |--|     \n     O  |     \n     |  |     \n     -  |     \n        |     \n        |     \n     ------   \n");
    }else if(healthRatioModulo<=4){
        printf("\n     |--|     \n     O  |     \n     |  |     \n        |     \n        |     \n        |     \n     ------   \n");
    }else if (healthRatioModulo<=5){
        printf("\n     |--|     \n     O  |     \n        |     \n        |     \n        |     \n        |     \n     ------   \n");
    }else if (healthRatioModulo<=6){
        printf("\n     |--|     \n        |     \n        |     \n        |     \n        |     \n        |     \n     ------   \n");
    }else if (healthRatioModulo<=7){
        printf("\n      --|     \n        |     \n        |     \n        |     \n        |     \n        |     \n     ------   \n");
    }else if (healthRatioModulo<=8){
        printf("\n              \n        |     \n        |     \n        |     \n        |     \n        |     \n     ------   \n");
    }else if (healthRatioModulo<=9){
        printf("\n              \n              \n              \n              \n              \n              \n     ------   \n");
    }
    //output
    printf("\nIl vous reste %d coup",gameDataPointer->healthPoints);
    printf( (gameDataPointer->healthPoints>1) ? "s " : " ");
#ifdef UNIX
    printf("à");
#endif
#ifdef WINDOWS
    printf("a");
#endif
    printf(" jouer\n");
    //show bad answers history
    if (gameDataPointer->healthPoints<10) {
        for (int healthPointsId=10-gameDataPointer->healthPoints; healthPointsId>0; healthPointsId-- ) {
            printf(" %c ", *(gameDataPointer->guessHistory+(healthPointsId-1)));
        }
    }
    printf("\n");
    printf("Quel est le mot secret ? ");
    promptSecret(gameDataPointer);
    printf("Proposez une lettre: ");
    return 0;
}

void promptSecret(GameData *gameDataPointer){
    for (int iSecretWord=0; iSecretWord<gameDataPointer->wordLength; iSecretWord++) {
        printf("%c",*(gameDataPointer->secretPrompt+iSecretWord));
    }
    printf("\n");
}

void correct(GameData *gameDataPointer){
    //Check every char of the word
    for (int iSecretWord=0; iSecretWord<gameDataPointer->wordLength; iSecretWord++) {
        //if it match
        if (gameDataPointer->guess==*(gameDataPointer->secretWord+iSecretWord)) {
            //replace the * by the good char
            *(gameDataPointer->secretPrompt+iSecretWord)=*(gameDataPointer->secretWord+iSecretWord);
            gameDataPointer->guessStatus=1;
        }
    }
}

char manageEOTry(GameData *gameDataPointer){
    if (gameDataPointer->healthPoints>0) {
        //browse in secretPrompt to check if there is still some *
        for (int iSecretWord=0; iSecretWord<gameDataPointer->wordLength; iSecretWord++) {
            if (*(gameDataPointer->secretPrompt+iSecretWord)=='*') {
                gameDataPointer->gameStatus='r';
                return 0;
            }
        }
        gameDataPointer->gameStatus='w';
        return 0;
    }
    gameDataPointer->gameStatus='o';
    return 0;
}
