//
//  utilities.c
//  tataPendu
//
//  Created by Tatadmound on 03/03/2021.
//
#include "../Headers/gameMaster.h"
#include "../Headers/utilities.h"
#include <stdlib.h>
#include <ctype.h>

char getUpperChar(){
    char userInput = 0;

    userInput = getchar(); // get the 1rst input
    userInput = toupper(userInput); // set uppercase if not

    while (getchar() != '\n') ; //clear the rest of the input

    return userInput;
}

char askBinaryInput(){
#ifdef WINDOWS
    printf("\nRepondez par oui (O) ou par non (N)");
#endif
#ifdef UNIX
    printf("\nRépondez par oui (O) ou par non (N)");
#endif
    char userInput = getUpperChar();
    if (userInput!='N' && userInput!='O' && userInput!='0') {
#ifdef WINDOWS
        printf("\nReponse incorect, fermeture du jeu.\n");
#endif
#ifdef UNIX
        printf("\nRéponse incorect, fermeture du jeu.\n");
#endif
        exit(0);
    }
    return userInput;
}

char askFigureInput(char bornInf, char bornMax){
    char userInput=0;
    do {
#ifdef WINDOWS
        printf("Veuillez repondre");
#endif
#ifdef UNIX
        printf("Veuillez répondre");
#endif
        printf(((bornMax - bornInf) == 1)? " par %c ou %c.\n" : " par une valeur comprise entre %c et %c.\n", bornInf, bornMax);
        scanf("%c", &userInput);
        fflush(stdin);
    } while (!(userInput>=bornInf && userInput <= bornMax));
    return userInput;
}


int getLineLength(FILE* file, int lineNumber){
    //get to the wanted line
    long linePosition=getLinePosition(file, lineNumber);
    fseek(file, linePosition, SEEK_SET);
    //count chars
    int charsCount=0;
    char currentChar=0;
    do {
        currentChar=getc(file);
        charsCount++;
    } while (currentChar!='\n');
    return charsCount;
}

double getLinePosition(FILE* file, int lineNumber){
    int lineCounter=0;
    char currentChar='0';
    if (file!=NULL) {
        lineCounter=countLinesInFile(file);
        if (lineCounter<lineNumber) {
            printf((DEV_MOD)? "\nLe fichier ne comporte que %d lignes, la ligne n°%d n'existe donc pas":"\nune erreur est survenue\ncode:%d%d???",lineCounter, lineNumber);
            return -1;
        }else{
            rewind(file);
            lineCounter=1;
            do {
                currentChar=getc(file);
                if (currentChar=='\n') {
                    lineCounter++;
                }
            } while (lineCounter!=lineNumber);
        }
    }else{
        printf((DEV_MOD)?"(getLinePosition) Le fichier n'est pas ouvert / n'existe pas\n":"\nune erreur est survenue\ncode:???");
    }
    return ftell(file);
}

char copyTheRestOfTheFile(FILE* originFile ,FILE* targetFile, long startPosition){
    fseek(originFile, startPosition, SEEK_SET);
    char currentChar=0;
    do {
        currentChar=fgetc(originFile);
        if (currentChar!=EOF) {
            fputc(currentChar, targetFile);
        }
    } while (currentChar!=EOF);
    return 0;
}

int countLinesInFile(FILE* file){
    rewind(file);
    int lineCounter=0;
    char currentChar='0';
    do{
        lineCounter++;
        currentChar=fgetc(file);
        while (currentChar!='\n' && currentChar!=EOF) {
            currentChar=fgetc(file);
        }
    }while (currentChar!=EOF);
    //ignore a possible empty last line
    fseek(file, -1, SEEK_CUR);
    currentChar=fgetc(file);
    if (currentChar=='\n') {
        lineCounter--;
    }
    return lineCounter;
}
