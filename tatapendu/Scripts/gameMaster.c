//
//  gameMaster.c
//  tataPendu
//
//  Created by Tatadmound on 20/02/2021.
//
#include "../Headers/gameMaster.h"
#include <stdlib.h>
#include <ctype.h>

void gameMaster(){
    //creating our gameMaster data struct
    GameData *gameDataPointer=NULL;
    gameDataPointer=malloc(sizeof(GameData));
    //creating ou dictionaryData struct
    DictionaryData *dictionaryDataPointer=NULL;
    dictionaryDataPointer=malloc(sizeof(DictionaryData));
    
    //testing the previous malloc succes
    //init return 0 if no error
    if(!init(gameDataPointer, dictionaryDataPointer)){
        //tryAgain() until gameData.gameStatus == 'r' ?
        do {
            tryAgain(gameDataPointer);
        } while (gameDataPointer->gameStatus == 'r');
        endGame(gameDataPointer, dictionaryDataPointer);
    }
}
