//
//  gameMaster.h
//  tataPendu
//
//  Created by Tatadmound on 20/02/2021.
//
//Toutes les fonctions des script de gameMaster
#ifndef gameMaster_h
#define gameMaster_h

#include <stdio.h>
#include <stdlib.h>
#include "../CustomTypes/gameData.h"
#include "../CustomTypes/dictionaryData.h"
#define DEV_MOD 0
//OS output optimization
#define UNIX
//#define WINDOWS

#endif /* gameMaster_h */
//Manage the game program
void gameMaster(void);
//store all the needed data into our gameData struct
char init(GameData *gameDataPointer, DictionaryData *dictionaryDataPointer);
    //init intern functions
    //Output a confirmation sentence about his previous choice
    void confirmBasicDictionaryStatusToUser(GameData* gameDataPointer, DictionaryData* dictionaryDataPointer);
    //manage dictionary init functions
    char initDictionary(GameData *gameDataPointer, DictionaryData *dictionaryDataPointer);
        //init basic Dictionary
        char initBasicDictionary(GameData *gameDataPointer, DictionaryData *dictionaryDataPointer);
            //correct format of basic issues (lenght of word, spaces)
            char cleanBasicDictionary(FILE* basicDictionary, GameData* gameDataPointer, DictionaryData* dictionaryDataPointer);
            //update the first line of basic dictionary file
            char updateWordsCountInBasicDictionary(FILE* basicDictionary, DictionaryData *dictionaryDataPointer);
            //create a new basicDictionary file
            void newDefaultBasicDictionary(void);
                //add words to have at least 20 words in basicDictionary
                char addWordsToBasicDictionary(FILE* dictionary, int wordsCount, DictionaryData* dictionaryDataPointer);
    //return a random word id
    int randomWord(DictionaryData *dictionaryDataPointer, GameData *gameDataPointer);
        //intern function for basicDictionary mod
        int randomWordInBasicDictionary(DictionaryData *dictionaryDataPointer);
        //set wordLength, secretWord & secretPrompt in our GameData struct
        char findWordInBasicDictionary(int wordId, GameData *gameDataPointer, DictionaryData* dictionaryDataPointer);
         
//Manage a round of a game
char tryAgain(GameData *gameDataPointer);
    //all the package of printf for the user
    char promptTry(GameData *gameDataPointer);
    //print (*gameDataPointer).secretPrompt + "\n"
    void promptSecret(GameData *gameDataPointer);
    //set gameData.guessStatus to 1 if good answer, 0 if not.
    //in case of good answer, replace '*' of concerned char(s) of gameData.secretPrompt
    void correct(GameData *gameDataPointer);
    //Check if the game is over. gameData.gameStatus => 'r' need to retry / 'w' game won / 'o' game lost
    char manageEOTry(GameData *gameDataPointer);
//Mange the end of the game (relaunch or quit)
void endGame(GameData *gameDataPointer, DictionaryData *dictionaryDataPointer);
