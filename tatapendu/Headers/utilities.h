//
//  utilities.h
//  tataPendu
//
//  Created by Tatadmound on 03/03/2021.
//
#ifndef utilities_h
#define utilities_h

#include <stdio.h>
#include <stdlib.h>

#endif /* utilities_h */

//utilities
    //return user's input as an uppercase
    char getUpperChar(void);
    //return the user input from a Yes/No question : 'N' for no / 'O' or '0' for Yes
    char askBinaryInput(void);
    //return the user input from a multiple choices question as an integer (max 10 choices)
    char askFigureInput(char bornInf, char bornMax);
    //return count words in basic dictionary
    int countLinesInFile(FILE* file);
    //return line lenght
    int getLineLength(FILE* file, int lineNumber);
    //travel to specific line and return his ftell position
    double getLinePosition(FILE* file, int lineNumber);
    //copy the rest of the file from the current location (file to copy, file to paste, position to start)
    char copyTheRestOfTheFile(FILE* originFile ,FILE* targetFile, long startPosition);
